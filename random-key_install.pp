class random-key_install {
  # assumes that the random-key service package is available in a registered repo
  # assumes that the package has the init.d file as well as the binary itself

  $port    = lookup( {"name" => "random-key::port", "default_value" => "8888"} )
  $version = lookup( {"name" => "random-key::version", "default_value" => "1.0.0"} )

  package { 'random-key':
    ensure => "${version}",
  }

  file { "/etc/default/random-key":
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    ensure  => present,
    content => "{\"port\": ${port}}",
    notify  => Service['random-key'],
  }

  service { 'random-key':
    ensure     => running,
    enable     => true,
    hasstatus  => true,
    hasrestart => true,
    require => Package['random-key'],
  }
}