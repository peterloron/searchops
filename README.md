SearchOps Assignment
====================

This repository contains material produced for the SearchOps assignment. 

Random-key Service
------------------
This is a simple service, written in Go, which listens for a specific RESTful request URL, and responds with the desired number of base64 encoded bytes of random data. The service defaults to listening on port 8080, unless another port is specified. A config file can be located at "/etc/default/random-key" or in the same directory as the service binary. The service will look for an environment variable named "RANDOMKEY_PORT" which can also be used to configure the service. The config file can be any format supported by the Golang Viper library.

The service listens on HTTP only as it is intended to be run behind a load balancer and ingress controller which handle TLS termination.

The service can be built using the included Makefile. The "make build" command will construct a statically linked Linux amd64 binary suitable for running in a scratch Docker container. A "make test" command is also available for running the unit tests.


Nagios Check
------------
A simple "check_http" example is in the "nagios_check" file. This is an example of how a probe might be written in Nagios to check aliveness as well as response time.


Puppet Class
------------
This should install the random-key service from a previously configured package repository, configure the "/etc/default/random-key" file, and ensure the service is installed, running, and configured to run at boot time. The version of the package installed as well as the port injected into the config file come from hiera data. The data in hiera can be structured to support deploying different package versions and/or port configurations in different environments.


Docker
------
The Dockerfile will build a minimum-ish size image using a scratch container. Since the binary has been statically compiled and has no other external dependencies (e.g TLS trusted certs), it is very simple. The lack of complexity and tiny size reduce startup times and storage costs, as well as reducing errors.


Chart
-----
This directory holds a Helm chart for deploying the random-key service into a Kubernetes cluster. It sets up an nginx-based ingress controller, as well as a horizontal pod autoscaler which allows some automatic capacity scaling. The HPA is configured to use pod CPU as the metric, but a real deployment would use a custom metrics server and look at multiple metrics. Kubernetes health checks call the valid endpoint to check functionality. A PDB is defined to help ensure the service remains up during rolling updates. Affinity rules are configured to help ensure that pods are distributed across zones.

The chart makes the following assumptions:

* Cert-manager (or equivalent) is installed in the cluster and configured with an issuer named "letsencrypt-prod".
* The Kubernetes maintained ingress-nginx is installed and configured for L4 routing.
* TLS termination is being handled by the nginx ingress pod.
* The service needs no special access, so no RBAC objects are defined.
* The cluster is deployed in an HA fashion across 3+ zones.
* Metrics-server is installed and configured.
* The desired namespace already exists.
* The DNS alias entry for the hostname has been created and pointed to the EBS DNS name.
