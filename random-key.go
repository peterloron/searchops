package main

import (
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/go-zoo/bone"
	"github.com/spf13/viper"
)

var port int

// GenerateRandomBytes produces the number of requested bytes of random data, base64 encoded.
func generateRandomBytes(w http.ResponseWriter, r *http.Request) {

	// extract number of bytes to generate from the request
	numBytes, err := strconv.Atoi(bone.GetValue(r, "numBytes"))
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Error!"))
		log.Printf("Error while getting input!\n%s", err)
		return
	} else if numBytes <= 0 {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Error!"))
		log.Print("Invalid input!")
		return
	}

	// use the crypto library to generate random data and insert it into the slice
	data := make([]byte, numBytes)
	if _, err := rand.Read(data); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Error!"))
		log.Printf("Error while getting random data!\n%s", err)
	} else {
		// base64 encode buffer and write it out
		ed := base64.StdEncoding.EncodeToString(data)
		w.Write([]byte(ed))
	}
}

func loadConfig() {
	viper.SetDefault("port", 8080)
	viper.SetConfigName("random-key") // name of config file (without extension)
	viper.AddConfigPath("/etc/default/")
	viper.AddConfigPath(".")
	viper.SetEnvPrefix("RANDOMKEY")
	viper.AutomaticEnv()

	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			// Config file not found; ignore error if desired
			log.Print("Config file not found. Using default port.")
		} else {
			// Config file was found but another error was produced
			log.Printf("Error while parsing config file!\n%s", err)
		}
	}
	// Config file found and successfully parsed
	port = viper.GetInt("port")
	log.Printf("Found port config. Using port: %d", port)
}

func main() {
	loadConfig()
	mux := bone.New()
	mux.Handle("/keys/:numBytes", http.HandlerFunc(generateRandomBytes))

	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", port), mux))
}
