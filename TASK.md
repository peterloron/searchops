Write an HTTP service in a language of your choice that generates a number of requested random bytes and returns the results Base64 encoded.



Assuming this service has a regular init.d start/stop script and is packaged in random-key_1.0.0.deb, write a Puppet class to:
a. deploy the package
b. ensure it is running
c. will restart on reboot

The init.d script has an option to specify the listening port of the service in /etc/default/random-key file with a simple shell variable such as: $LISTEN_PORT. Augment the Puppet manifest to update this file from a hiera variable.

Write a health check script that can be run from Nagios to test this service.

Write a Dockerfile and Kubernetes (K8s) manifest that would deploy this service into K8s. Solution should include ingress setup (state any assumptions made about the cluster)

Write a health check for this K8s service


