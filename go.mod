module random-key

go 1.12

require (
	github.com/go-zoo/bone v1.3.0
	github.com/spf13/viper v1.4.0
)
