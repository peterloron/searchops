package main

import (
	"encoding/base64"
	"fmt"
	"math"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-zoo/bone"
)

var pathTests = []struct {
	p  string // input
	sc int    // expected status code
}{
	{"", 404},
	{"/", 404},
	{"/foo", 404},
	{"/keys", 404},
	{"/keys/", 400},
	{"/keys/10/", 302},
	{"/keys/10/gibberish", 404},
}

var intTests = []struct {
	n  int // input
	sc int // expected status code
}{
	{1, 200},
	{12, 200},
	{1024, 200},
	{0, 400},
	{-1, 400},
	{math.MaxInt16, 200},
}

func TestGenerateRandomBytesBadPath(t *testing.T) {
	t.Parallel()
	mux := bone.New()

	for _, table := range pathTests {
		rr := httptest.NewRecorder()
		req, err := http.NewRequest("GET", fmt.Sprintf("%s", table.p), nil)
		if err != nil {
			t.Fatal(err)
		}

		mux.Handle("/keys/:numBytes", http.HandlerFunc(generateRandomBytes))
		mux.ServeHTTP(rr, req)

		// Check the status code
		if rr.Code != table.sc {
			t.Errorf("Wrong status code: Received: %v, Expected: %v, Path: %s", rr.Code, table.sc, table.p)
		}
	}
}

func TestGenerateRandomBytesInt(t *testing.T) {
	t.Parallel()
	mux := bone.New()

	for _, table := range intTests {
		rr := httptest.NewRecorder()
		req, err := http.NewRequest("GET", fmt.Sprintf("/keys/%d", table.n), nil)
		if err != nil {
			t.Fatal(err)
		}

		mux.Handle("/keys/:numBytes", http.HandlerFunc(generateRandomBytes))
		mux.ServeHTTP(rr, req)

		// Check the status code
		if rr.Code != table.sc {
			t.Errorf("Wrong status code: Received: %v, Expected: %v, Input: %d", rr.Code, table.sc, table.n)
		}

		// Check that the response body is the size we expect.
		if rr.Code == http.StatusOK {
			dd, err := base64.StdEncoding.DecodeString(rr.Body.String())
			if err != nil {
				t.Fatal(err)
			} else if len(dd) != table.n {
				t.Errorf("Error! Got %d bytes! Expected %d", len(dd), table.n)
			}
		}
	}
}
