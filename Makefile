clean:
	rm random-key

build:
	go mod tidy
	GOOS=linux GO_ARCH=x64 CGO_ENABLED=0 go build -a -ldflags '-extldflags "-static"' -o random-key 

test:
	go test
